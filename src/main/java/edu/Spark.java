package edu;

import static spark.Spark.get;
import static spark.Spark.post;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

public class Spark {

	static List<Producto> productos;

	public static void main(String[] args) {

		get("/", (request, response) -> {
			Map<String, Object> model = new HashMap<>();
			model.put("welcome", " Bienvenido a Quierelo");
			return new ModelAndView(model, "index.wm");
		}, new VelocityTemplateEngine());

		get("/buscarproducto", (request, response) -> {
			Map<String, Object> model = new HashMap<>();
			model.put("direcciones", " ");

			return new ModelAndView(model, "buscarproducto.wm");
		}, new VelocityTemplateEngine());

		post("/buscar",
				(request, response) -> {
					Map<String, Object> model = new HashMap<>();

					String parametro = request.queryParams("nombre");
					ProductoBean pb = new ProductoBean();
					String respuesta = pb.buscarProductoPorNombre(parametro);
					model.put("direcciones",
							respuesta);
					return new ModelAndView(model, "buscarproducto.wm");
				}, new VelocityTemplateEngine());

		get("/registroproducto", (request, response) -> {
			Map<String, Object> model = new HashMap<>();
			model.put("mensaje", "");
			return new ModelAndView(model, "registroproducto.wm");
		}, new VelocityTemplateEngine());

		post("/registrar", (request, response) -> {
			Map<String, Object> model = new HashMap<>();

			String nombre = request.queryParams("nombre");
			String descripcion = request.queryParams("descripcion");
			String talla = request.queryParams("talla");
			String valor = request.queryParams("valor");
			String tienda = request.queryParams("tienda");

			Producto producto = new Producto(nombre, descripcion, talla, valor,
					tienda);

			ProductoBean pb = new ProductoBean();
			String respuesta = pb.registrarproducto(producto);

			model.put("mensaje", respuesta);
			return new ModelAndView(model, "registroproducto.wm");
		}, new VelocityTemplateEngine());

	}

}
