package edu;

import java.util.ArrayList;

public class ProductoBean {

	
	private static ArrayList<Producto> productos = new ArrayList<Producto>();

	public String registrarproducto (Producto producto){
		
		   
        
        String mensaje;
		if (producto.getNombre() != "" && producto.getDescripcion() != "" && producto.getTalla() != ""
                && producto.getValor() != "") {
           
            productos.add(producto);
            mensaje="su producto ha sido registrado satisfactoriamente";
        } else {
            mensaje="Error, Favor llenar los campos";
        }
		return mensaje;
		
	}

	public String buscarProductoPorNombre(String nombre) {
		String resppuesta = "";
		for (Producto producto : productos) {
			if(producto.getNombre().contains(nombre)){
				
				resppuesta= resppuesta +producto+" ";
				
			}
				
		}
		if(resppuesta.equals(""))
			resppuesta="No se encontro producto";
		
		return resppuesta;
	}
}
