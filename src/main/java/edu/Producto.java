package edu;

public class Producto {

	private String nombre;
	private String descripcion;
	private String talla;
	private String valor;
	private String tienda;

	public Producto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Producto(String nombre, String descripcion, String talla,
			String valor, String tienda) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.talla = talla;
		this.valor = valor;
		this.tienda = tienda;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTalla() {
		return talla;
	}

	public void setTalla(String talla) {
		this.talla = talla;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "Producto [nombre=" + nombre + " tienda =" + tienda + "]";
	}

	public String getTienda() {
		return tienda;
	}

	public void setTienda(String tienda) {
		this.tienda = tienda;
	}

}
