package edu;

import static org.junit.Assert.*;

import org.junit.Test;

public class ProductoTest {

	@Test
	public void siregistroproductodeboverseharegistradobien() {

		Producto producto = new Producto("zapatos", "mas zapatos", "38",
				"50.000", "AC");

		ProductoBean pb = new ProductoBean();
		String respuesta = pb.registrarproducto(producto);

		String esperado = "su producto ha sido registrado satisfactoriamente";
		assertEquals(esperado, respuesta);

	}

	@Test
	public void siNoingresoDatosregistroproductodebovererror() {

		Producto producto = new Producto("zapatos", "", "38", "50.000", "AC");

		ProductoBean pb = new ProductoBean();
		String respuesta = pb.registrarproducto(producto);

		String esperado = "Error, Favor llenar los campos";
		assertEquals(esperado, respuesta);

	}

	@Test
	public void siBuscarZapatosListarZapatos() {

		Producto producto = new Producto("zapatos", "mas zapatos", "38",
				"50.000", "AC");

		Producto producto1 = new Producto("zapatos grandes", "mas zapatos",
				"38", "50.000", "AC");

		Producto producto2 = new Producto("zapatos pequeños", "mas zapatos",
				"38", "50.000", "AC");

		ProductoBean pb = new ProductoBean();
		pb.registrarproducto(producto);

		pb.registrarproducto(producto1);

		pb.registrarproducto(producto2);

		String respuesta = pb.buscarProductoPorNombre("zapatos");

		String esperado = "Producto [nombre=zapatos tienda =AC] Producto [nombre=zapatos tienda =AC] Producto [nombre=zapatos grandes tienda =AC] Producto [nombre=zapatos pequeños tienda =AC] ";

		assertEquals(esperado, respuesta);

	}

	@Test
	public void siBuscartenisrespondernohayListarZapatos() {

		Producto producto = new Producto("zapatos", "mas zapatos", "38",
				"50.000", "AC");

		Producto producto1 = new Producto("zapatos grandes", "mas zapatos",
				"38", "50.000", "AC");

		Producto producto2 = new Producto("zapatos pequeños", "mas zapatos",
				"38", "50.000", "AC");

		ProductoBean pb = new ProductoBean();
		pb.registrarproducto(producto);

		pb.registrarproducto(producto1);

		pb.registrarproducto(producto2);

		String respuesta = pb.buscarProductoPorNombre("tenis");

		String esperado = "No se encontro producto";

		assertEquals(esperado, respuesta);

	}

}
