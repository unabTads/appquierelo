package edu;

import com.codeborne.selenide.Selenide;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class RegistroProductoTest {

	@Given("^ingreso a la opción registrar mi producto$")
	public void ingreso_a_la_opción_registrar_mi_producto() throws Throwable {
		// Express the Regexp above with the code you wish you had
		Selenide.open("http://localhost:4567");
		$(By.id("registrarproducto")).click();
	}

	@When("^ingrese los datos del producto")
	public void ingrese_el_nombre_del_producto_la_Descripción_del_producto_tallas_disponibles_y_el_valor_del_producto()
			throws Throwable {
		// Express the Regexp above with the code you wish you had
		// throw new PendingException();
		$(By.id("nombre")).setValue("Botas V");
		$(By.id("descripcion")).setValue("xxxxxxxxxxxxxxxxxxxxxx");
		$(By.id("talla")).setValue("85822");
		$(By.id("valor")).setValue("35461");
	}

	@When("^doy registrar$")
	public void doy_registrar() throws Throwable {
		// Express the Regexp above with the code you wish you had
		$(By.id("registrar")).click();
	}

	@Then("^debo ver \"([^\"]*)\".$")
	public void debo_ver_(String arg1) throws Throwable {
		$(By.id("mensaje")).shouldHave(
				text(arg1));
	}

	
}
