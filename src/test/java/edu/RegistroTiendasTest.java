package edu;

import com.codeborne.selenide.Selenide;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;


public class RegistroTiendasTest {

	@Given("^ingreso a la opción registrar mi tienda$")
	public void ingreso_a_la_opción_registrar_mi_tienda() throws Throwable {
		$(By.id("registrartienda")).click();
	}

	@When("^ingrese el nombre de mi tienda \"([^\"]*)\" y la ciudad de ubicación \"([^\"]*)\"$")
	public void ingrese_el_nombre_de_mi_tienda_y_la_ciudad_de_ubicación(String arg1, String arg2) throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    throw new PendingException();
	}

	@Then("^debo ver su tienda \"([^\"]*)\" ha sido registrada satisfactoriamente.$")
	public void debo_ver_su_tienda_ha_sido_registrada_satisfactoriamente(String arg1) throws Throwable {
	    // Express the Regexp above with the code you wish you had
	    throw new PendingException();
	}

	
}
