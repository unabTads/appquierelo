package edu;

import com.codeborne.selenide.Selenide;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;


public class BuscadorPorProductoTest {
	
	@Given("^estoy en el index$")
	public void estoy_en_el_index() throws Throwable {
	    // Express the Regexp above with the code you wish you had
		  Selenide.open("http://localhost:4567/");
	}

	@When("^presiono buscar producto$")
	public void presiono_buscar_producto() throws Throwable {
	    // Express the Regexp above with the code you wish you had
		$(By.id("buscarproducto")).click();
	}



	@When("^ingreso el nombre del producto \"([^\"]*)\"$")
	public void ingreso_el_nombre_del_producto(String nombre) throws Throwable {
	    // Express the Regexp above with the code you wish you had
		   $(By.id("nombre")).setValue(nombre);
	}

	@When("^doy Buscar$")
	public void doy_Buscar() throws Throwable {
	    // Express the Regexp above with the code you wish you had
		$(By.id("buscar")).click();
	}


	
	@Then("^debo ver las direcciones donde encuentro el producto \"([^\"]*)\"$")
	public void debo_ver_las_direcciones_donde_encuentro_el_producto(String direcciones) throws Throwable {
	    // Express the Regexp above with the code you wish you had
		 $(By.id("direcciones")).shouldHave(text(direcciones));
	}

	

}
