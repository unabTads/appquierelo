Feature:
	como dueño de la tienda
	quiero registrar mis productos en la aplicación
	para que puedan ser consultados por los compradores 
	
Scenario:
	Given ingreso a la opción registrar mi producto
	When ingrese los datos del producto
	And doy registrar
	Then debo ver "su producto ha sido registrado satisfactoriamente".