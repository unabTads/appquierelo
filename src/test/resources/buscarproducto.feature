Feature:
	como cliente de la tienda
	quiero buscar un producto
	para saber en que tienda más cercana está 
	
Scenario:
	Given estoy en el index
	When presiono buscar producto
	And ingreso el nombre del producto "Botas Velez"
	And doy Buscar
	Then debo ver las direcciones donde encuentro el producto "Centro Comercial Cañaveral, Centro Comercial Parque Caracoli"